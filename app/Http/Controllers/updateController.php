<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Schema;
use App\Quotation;
use Carbon\Carbon;

class updateController extends Controller
{
    public function actualizarAlumnos(){
    	return view('update.listarAlumnos');
    }

    public function CamposActualizar(Request $request){
    	$alumno = DB::table('alumnos')->where('idAlumno','=',$request['id'])->get();
    	return view('update.camposActualizar',compact('alumno'));
    }

    public function actualizarCampos(Request $request){

    	DB::table('alumnos')->where('idAlumno', $request['idUp'])->update([
    		'nombres' => $request['nombres'],
    		'correo' => $request['email'],
    		'telefono' => $request['telefono'],
    		'asignatura_idCurso' => $request['curso'],
    	]);
    	return view('update.listarAlumnos');
    }
}

