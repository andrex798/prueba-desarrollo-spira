<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Schema;
use App\Quotation;
use Carbon\Carbon;

class readController extends Controller
{
    public function alumnos(){
    	$alumnos = DB::table('alumnos')->join('asignatura','asignatura_idCurso','=','asignatura.idCurso')->get();

    	return $alumnos;
    }

    public function cursos(){
    	$cursos = DB::table('asignatura')->get();

    	return $cursos;
    }

    public function listarAlumnos(){
    	return view('read.listarAlumnos');
    }
}
