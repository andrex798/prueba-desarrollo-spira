<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Schema;
use App\Quotation;
use Carbon\Carbon;



class createController extends Controller
{
    public function crear(){
    	return view('create.crearAlumno');
    }

    public function crearEstudiante(Request $request){
    	DB::table('alumnos')->insert([
    		'nombres' => $request['nombres'],
    		'correo' => $request['email'],
            'telefono' => $request['telefono'],
            'asignatura_idCurso' => $request['curso'],

    	]);
    	return redirect('crearAlumno');
    }
}
