@extends('layouts.principal')

@section('content')

{!!Form::open(['action' => 'updateController@actualizarCampos', 'method' => 'POST'])!!}
@foreach($alumno as $alumno)
	  <div class="form-group">
	    <label for="inputAddress">Nombres</label>
	    <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombres Completos" required="required" value="{{$alumno->nombres}}">
	  </div>
	  <div class="form-group">
	    <label for="inputAddress">Email</label>
	    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required" value="{{$alumno->correo}}">
	  </div>
	  <div class="form-group">
	    <label for="inputAddress">Teléfono</label>
	    <input type="number" class="form-control" id="telefono" name="telefono" placeholder="Numero Telefonico" required="required" value="{{$alumno->telefono}}">
	  </div>
	  <div class="form-group">
	    <label for="inputAddress">Curso</label>
	    <select class="custom-select cursos" name="curso">
		  <option selected>Seleccione un Curso</option>
		</select>
	  </div>

	  <input type="text" name="idUp" value="{{$alumno->idAlumno}}" style="display: none;">
@endforeach	  
	  <button type="submit" class="btn btn-primary enviar">Enviar</button>
	{!!Form::close()!!}

@endsection