@extends('layouts.principal')


@section('content')

<div class="container-fluid">

	{!!Form::open(['action' => 'createController@crearEstudiante', 'method' => 'POST'])!!}
	  <div class="form-group">
	    <label for="inputAddress">Nombres</label>
	    <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombres Completos" required="required">
	  </div>
	  <div class="form-group">
	    <label for="inputAddress">Email</label>
	    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required">
	  </div>
	  <div class="form-group">
	    <label for="inputAddress">Teléfono</label>
	    <input type="number" class="form-control" id="telefono" name="telefono" placeholder="Numero Telefonico" required="required">
	  </div>
	  <div class="form-group">
	    <label for="inputAddress">Curso</label>
	    <select class="custom-select cursos" name="curso">
		  <option selected>Seleccione un Curso</option>
		</select>
	  </div>
	  <button type="submit" class="btn btn-primary enviar">Enviar</button>
	{!!Form::close()!!}

</div>

@endsection