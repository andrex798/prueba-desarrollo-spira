<!DOCTYPE html>
<html>
<head>
	<title>Prueba desarrollo Jorge Rodriguez</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link href="../public/css/sb-admin.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</head>
<body>


<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="">Prueba Desarrollo Spira</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>




  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
 
      <li class="nav-item">
        <a class="nav-link" href="../public/crearAlumno">
          <i class="material-icons">note_add</i>
          <span>Crear Alumno</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../public/listarAlumnos">
          <i class="material-icons">list_alt</i>
          <span>Listar Alumnos</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../public/actualizarAlumnos">
          <i class="material-icons">edit</i>
          <span>Actualizar Alumnos</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../public/actualizarAlumnos">
          <i class="material-icons">delete</i>
          <span>Eliminar Alumno</span></a>
      </li>

    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">
       @yield('content')


      <!-- Sticky Footer -->
 

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

<script src="../public/js/main.js"></script>
<script src="../public/js/sb-admin.min.js"></script>
</body>
</html>