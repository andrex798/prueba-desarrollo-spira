$(document).ready(function(){
	
	 $.ajax({
              type: "GET",
              url: "alumnos",
              success: function(data)
              {
              
              var alumnos = data;
              console.log(data);
              for(i=0;i<data.length;i++){

              var listaAlumnoUpdate = "<tr>";
              listaAlumnoUpdate += "<td>"+data[i].nombres+"</td>";
              listaAlumnoUpdate += "<td><a href='../public/camposActualizar?id="+data[i].idAlumno+"' class='btn btn-info'>Actualizar</a> <a href='../public/eliminar?id="+data[i].idAlumno+"' class='btn btn-danger'>Eliminar</a></td></tr>";
              $('.listAlumnosUpdate').append(listaAlumnoUpdate);
             


              var listaAlumno = "<tr>";
              listaAlumno += "<th scope='row'>"+data[i].idAlumno+"</th>";
              listaAlumno += "<td>"+data[i].nombres+"</td>";
              listaAlumno += "<td>"+data[i].correo+"</td>";
              listaAlumno += "<td>"+data[i].telefono+"</td>";
              listaAlumno += "<td>"+data[i].nombreAsignatura+"</td>";

              $('.listaAlumno').append(listaAlumno);
              }
              
            }
          });


	 $.ajax({
              type: "GET",
              url: "cursos",
              success: function(data)
              {
              
              var cursos = data;
              
              for(i=0;i<cursos.length;i++){
              	console.log(cursos[i].nombreAsignatura);
              $('.cursos').append("<option value='"+cursos[i].idCurso+"'>"+cursos[i].nombreAsignatura+"</option>");
              }
              
            }
          });

   $("#filtrar").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $(".table tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });

   if ($('.cursos').val() == 'Seleccione un Curso') {
   $('.enviar').prop("disabled",true);
   }

   $(".cursos").change(function () { 
    var curso = $(this).val();
    if (curso != 'Seleccione un Curso') {
      $('.enviar').prop("disabled",false);
    }else{
      $('.enviar').prop("disabled",true);
    }

                  
    });

});