<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('create.crearAlumno');
});

//createController
Route::get('/crearAlumno', 'createController@crear')->name('crear');//->middleware('auth')
Route::post('/crearEstudiante', 'createController@crearEstudiante')->name('crearEstudiante');

//updateController
Route::get('/actualizarAlumnos', 'updateController@actualizarAlumnos')->name('actualizarAlumnos');//->middleware('auth')
Route::get('/camposActualizar', 'updateController@camposActualizar')->name('camposActualizar');//->middleware('auth')
Route::post('/actualizarCampos', 'updateController@actualizarCampos')->name('actualizarCampos');

//readController
Route::get('/alumnos', 'readController@alumnos')->name('alumnos');
Route::get('/cursos', 'readController@cursos')->name('cursos');
Route::get('/listarAlumnos', 'readController@listarAlumnos')->name('listarAlumnos');

//deleteController
Route::get('/eliminar', 'deleteController@eliminar')->name('eliminar');